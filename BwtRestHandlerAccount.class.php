<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: BwtRestHandlerAccount.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: BwtRestHandlerAccount.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("BwtConnection.php");
require_once("BwtLogger.php");
require_once("JwtAuth.php");
require_once("User.class.php");
require_once("UserModel.class.php");
require_once("BwtStorylineModel.class.php");
require_once("BwtStorylinePubModel.class.php");
require_once("BwtCookieConsentModel.class.php");

/**
 * Description of BwtRestHandlerAccount
 *
 * @author IZIordanov
 */
class BwtRestHandlerAccount extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option()
    {
        $mn = "BwtRestHandlerAccount::Option()";
        $response = new Response("success", "Service working.");

        $rh = new BwtRestHandlerAccount();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "BwtRestHandlerAccount::Ping()";
        BwtLogger::logBegin($mn);
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            if (isset($conn)) {
                BwtLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="JWT Tocken Methods">
    
    public function Login($email, $password)
    {
        $mn = "BwtRestHandlerAccount::Login('.$email.', '.$password.')";
        BwtLogger::logBegin($mn);
        $response = null;
        try {
            $user = User::login($email, $password);
            if (isset($user)) {
                BwtLogger::log($mn, " user = " . $user->toJson());
                $currUser = new UserModel($user);
                if (JwtAuth::signTocken($user->getId(), $currUser)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("current_user", $currUser);
                    $refreshToken = JwtAuth::geRrefreshTockenByUserId($user->getId());
                    $response->addData("refreshToken", $refreshToken);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function refreshToken($refresh)
    {
        $mn = "BwtRestHandlerAccount::refreshToken()";
        BwtLogger::logBegin($mn);
        $response = null;
        try {
            $user = JwtAuth::RefreshTocken($refresh);
            //BwtLogger::log($mn, " user = " .json_encode($user));
             if (isset($user)) {
                  $currUser = BwtUser::LoadById($user->getId());
                
                if (JwtAuth::signTocken($user->getId(), $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("current_user", $currUser);
                    $refreshToken = JwtAuth::geRrefreshTockenByUserId($user->getId());
                    $response->addData("refreshToken", $refreshToken);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 412;
                }
            } else {
                $response = new Response("error", "Invalid Refresh Token.");
                $response->statusCode = 412;
            }
            

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    
    
    // <editor-fold defaultstate="collapsed" desc="BwtUser Methods New">

    public static function decryptPwd($mn, $encrypted){
        $key = pack("H*", "0123456789abcdef0123456789abcdef");
        $iv =  pack("H*", "abcdef9876543210abcdef9876543210");
        $password = null;
        try {
            $encrypted2 = base64_decode($encrypted);
            $password1 =  mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted2, MCRYPT_MODE_CBC, $iv);
            //BwtLogger::log($mn, " password1 = " . $password1);
            $pad = substr($password1, -1);
            $password = rtrim($password1, $pad);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }
        return $password;
    }
    
    public function BwtLogin($email, $encrypted )
    {
        $mn = "bwtLogin()";
        BwtLogger::logBegin($mn);
        $response = null;
        //BwtLogger::log($mn, " email = " . $email);
        try {
            $password = BwtRestHandlerAccount::decryptPwd($mn, $encrypted);
            //BwtLogger::log($mn, " password = " . $password);
            $user = BwtUser::login($email, $password);
            //BwtLogger::log($mn, " user = " . json_encode($user));
            if (isset($user)) {
                //BwtLogger::log($mn, " user = " . json_encode($user));
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                //BwtLogger::log($mn, " ipAddress = " . $ipAddress);
                $user->ipAddress = $ipAddress;
                $user = BwtUser::Save($user);
                if (JwtAuth::signTocken($user->id, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("current_user", $user);
                    $refreshToken = JwtAuth::geRrefreshTockenByUserId($user->id);
                    $response->addData("refreshToken", $refreshToken);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function BwtUserSave($user )
    {
        $mn = "BwtRestHandlerAccount::BwtUserSave()";
        BwtLogger::logBegin($mn);
        $response = null;
        
        try {
            BwtLogger::log($mn, " user = " . json_encode($user));
            if (isset($user) && isset($user->id)) {
                //BwtLogger::log($mn, " user = " . json_encode($user));
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                //$ipAddress = preg_replace('/(^"\\"|\\""$)/', '', $ipAddress);
                //BwtLogger::log($mn, " user = " . json_encode($user));
                $user->ipAddress = $ipAddress;
                $user = BwtUser::Save($user);
                $user->password = null;
                $response = new Response("success", "User Updated");
                $response->addData("current_user", $user);
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function BwtChangePassword($data )
    {
        $mn = "BwtRestHandlerAccount::BwtChangePassword()";
        BwtLogger::logBegin($mn);
        $response = null;
        
        try {
            BwtLogger::log($mn, " data = " . json_encode($data));
            $password = BwtRestHandlerAccount::decryptPwd($mn, $data->password);
            
            if (isset($data) && isset($data->id) && isset($password)) {
                $userIn = BwtUser::LoadById($data->id);
                $userIn->password = $password;
                //BwtLogger::log($mn, " user = " . json_encode($user));
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                //$ipAddress = preg_replace('/(^"\\"|\\""$)/', '', $ipAddress);
                //BwtLogger::log($mn, " user = " . json_encode($user));
                $userIn->ipAddress = $ipAddress;
                $user = BwtUser::ChangePassword($userIn);
                $response = new Response("success", "Password Updated");
                $response->addData("current_user", $user);
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function BwtRegister($eMail, $pwd)
    {
        $mn = "BwtRestHandlerAccount::BwtRegister -> ";
        BwtLogger::logBegin($mn);
        $response = null;
        $errMsg;
        BwtLogger::log($mn, " eMail = " . $eMail);
        try {
            $wrongEmail = false;
            $password = BwtRestHandlerAccount::decryptPwd($mn, $pwd);
            if(!isset($eMail) || !isset($password)){
                $wrongEmail = true;
            }
             
            if(!$wrongEmail ){
                $wrongEmail = !checkEmail($eMail);
                BwtLogger::log($mn, "checkEmail = " . $wrongEmail?'true':'false');
            }
            
            if(((bool)$wrongEmail) == FALSE){
                $conn = BwtConnection::dbConnect();
                $logModel = BwtLogger::currLogger()->getModule($mn);
                
                $objArrJ = BwtUser::CheckEmailJson($eMail, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                    $val = json_decode(json_encode($objArrJ[0]));
                     BwtLogger::log($mn, " val = " . json_encode($val));
                     $wrongEmail = (($val->rowCount > 0)?true:false);
                 }
                if(((bool)$wrongEmail)){
                     $errMsg = "E-Mail already registered.";
                }
            }
            
            BwtLogger::log($mn, "wrongEmail = " . $wrongEmail);
            if (!$wrongEmail) {
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $payload = [
                    'email' => $eMail, 
                    'password' => $password, 
                    'ipAddress'=> $ipAddress,
                    ];
                BwtLogger::log($mn, " payload = " . json_encode($payload));
                $dataJson = json_encode($payload);
                $val = json_decode($dataJson);
                $user = BwtUser::Save($val);
                
                BwtLogger::log($mn, "new user = " . json_encode($user));
                if (JwtAuth::signTocken($user->id, $user)) {
                    $response = new Response("success", "Registration succesfull.");
                    $response->addData("current_user", $user);
                    $refreshToken = JwtAuth::geRrefreshTockenByUserId($user->id);
                    $response->addData("refreshToken", $refreshToken);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                if(!isset($errMsg))
                {
                    $errMsg = "Invalid values provided";
                }
                $response = new Response("error", $errMsg);
                $response->statusCode = 200;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User Methods">
    
    public function Register($eMail, $password)
    {
        $mn = "BwtRestHandlerAccount::register('.$eMail.', '.$password.)";
        BwtLogger::logBegin($mn);
        $response = null;
        
        try {
            $wrongEmail = false;
            if(!isset($eMail) || !isset($password)){
                $wrongEmail = true;
            }
             
            if(!$wrongEmail ){
                $wrongEmail = !checkEmail($eMail);
                BwtLogger::log($mn, "checkEmail = " . $wrongEmail?'true':'false');
            }
            
            if(!$wrongEmail){
                $wrongEmail=User::isExistEMail($eMail);
                if($wrongEmail){
                     $response = new Response("error", "E-Mail already registered.");
                     $response->statusCode = 200;
                }
            }
            
            
            if (!$wrongEmail) {
                $user = User::CreateRowData($eMail, $password);
                
                BwtLogger::log($mn, "new user = " . $user->toJSON());
                 if (JwtAuth::signTocken($user->id, $user->getName())) {
                    $response = new Response("success", "JwtAuth Set.");
                    $currUser = new UserModel($user);

                    $response->addData("current_user", $currUser);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode = 200;
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function Autor($id) {
        $mn = "BwtBookRestHandler::Autor(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT SQL_CACHE user_id, e_mail, 
                is_receive_emails, user_name
                FROM iordanov_bwt.bwt_user
                where user_id=?";
            $bound_params_r = ["i", $id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("book_autor",$ret_json_data);
            
        } catch (Exception $ex) {
             BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="Public Data Access Methods">
    
    public function StorylinePubViewSave($value) {
        $mn = "BwtRestHandlerAccount::StorylinePubViewSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubViewModel::Save($value);
            $response = new Response("success", "BwtStorylinePubViewModel data published.");
            $response->addData("storylinepubview", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylinePubGetById($value) {
        $mn = "BwtBookRestHandler::StorylinePubGet(".$value.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubModel::LoadByStlId($value);
            $response = new Response("success", "BwtStorylinePubModel get.");
            $response->addData("storyline", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="Bwt CookieConsent Methods">
    
    public function BwtCookieConsentSave($value) {
        $mn = "BwtRestHandlerAccount::BwtCookieConsentSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtCookieConsentModel::Save($value);
            $response = new Response("success", "BwtCookieConsentModel data saved.");
            $response->addData("consent", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BwtCookieConsentGetById($value) {
        $mn = "BwtBookRestHandler::BwtCookieConsentGetById(".$value.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtCookieConsentModel::LoadById($value);
            $response = new Response("success", "BwtCookieConsentModel get.");
            $response->addData("consent", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
