<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : BwtChapterController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') . $delim . '../' .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/bwt' . $delim . '/home/iordanov/common/lib/bwt/models' .
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "BwtChapterController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("BwtLogger.php");
require_once("Functions.php");
require_once("BookModel.class.php");
require_once("BwtChapterModel.class.php");

require_once("BwtChapterRestHandler.class.php");
BwtLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
BwtLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new BwtChapterRestHandler();
    $restHendler->Option();
    BwtLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
   $method = $_SERVER['REQUEST_METHOD'];
   BwtLogger::log($mn, "method=" . $method);
    // read JSon input
    $payloadJson;
    $payloadAuth;
    $actor;
    $actor_id;
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    
    $authRes = JwtAuth::Autenticate();
    
     if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
            if(isset($payloadAuth)){
                $actor_id = $payloadAuth->data->user_id;
                $actor = $payloadAuth->data->user;
                BwtLogger::log($mn, "ping actor_id: ".$actor_id);
                BwtLogger::log($mn, "ping actor".json_encode($actor));
            }
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new BwtChapterRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new BwtChapterRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
    try{
        $payload = file_get_contents('php://input');
        if(isset($payload)){
            $payloadJson = json_decode($payload);
            BwtLogger::log($mn, "payloadJson=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn,  $ex);
    }
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new BwtChapterRestHandler();
            $restHendler->Ping($id);
            BwtLogger::log($mn, "ping executed");
            break;
        
        case "bwt_chapter_get":
            $rh = new BwtChapterRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ChapterGet($dataJson->id);
            }
            break;
            
        case "bwt_chapter_save":
            $rh = new BwtChapterRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ChapterSave($dataJson->chapter);
            }
            break;
        
        case "bwt_chapter_delete":
            $rh = new BwtChapterRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->ChapterDelete($dataJson->id);
            }
            break;
            
        case "chapter_table":
            // to handle REST Url /pcpd/
            $rh = new BwtChapterRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                BwtLogger::log($mn, " qry_limit: " . $payload_json->limit . " ");
                if (isset($payload_json)){
                    $response = BwtChapterModel::ChapterTable($payload_json, $actor_id);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        default:
            BwtLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


BwtLogger::logEnd($mn);

