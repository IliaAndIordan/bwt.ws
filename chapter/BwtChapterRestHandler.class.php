<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: BwtChapterRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: BwtChapterRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("BwtConnection.php");
require_once("BwtLogger.php");
require_once("JwtAuth.php");
require_once("User.class.php");
require_once("UserModel.class.php");
require_once("Character.class.php");
require_once("Chapter.class.php");
require_once("Book.class.php");
require_once("BookModel.class.php");
require_once("BwtChapterModel.class.php");


/**
 * Description of BwtChapterRestHandler
 *
 * @author IZIordanov
 */
class BwtChapterRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">

    public function Option()
    {
        $mn = "BwtChapterRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new BwtChapterRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "BwtChapterRestHandler::Ping()";
        BwtLogger::logBegin($mn);
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            if (isset($conn)) {
                BwtLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Chapter">
    
    public function ChapterGet($value) {
        $mn = "BwtChapterRestHandler::ChapterGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtChapterModel::LoadById($value);
            $response = new Response("success", "BwtChapterModel get.");
            $response->addData("chapter", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ChapterSave($value) {
        $mn = "BwtChapterRestHandler::ChapterSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtChapterModel::Save($value);
            $response = new Response("success", "Charter data saved.");
            $response->addData("chapter", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function ChapterDelete($id) {
        $mn = "BwtChapterRestHandler::ChapterDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtChapterModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Charter deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
