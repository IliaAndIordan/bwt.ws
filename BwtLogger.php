<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $_bwtLogger;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__) . '/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class BwtLogger extends LoggerBase
{
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct()
    {
        parent::__construct();
        $this->MN = "BwtLogger: ";
        try {


            $this->logger = Logger::getRootLogger();

            $this->logger->debug("BwtLogger init");

        } catch (Exception $ex) {
            echo "BwtLogger Error: " . $ex . "<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function loggerWad()
    {
        global $_bwtLogger;
        if (!isset($_bwtLogger)) {
            $_bwtLogger = new BwtLogger();
        }

        return $_bwtLogger;
    }

    public static function currLogger()
    {
        global $_bwtLogger;
        if (!isset($_bwtLogger)) {
            $_bwtLogger = new BwtLogger();
        }

        return $_bwtLogger;
    }

    public static function logBegin($mn)
    {
        BwtLogger::currLogger()->begin($mn);
    }

    public static function logEnd($mn)
    {
        BwtLogger::currLogger()->end($mn);
    }

    public static function log($mn, $msg)
    {
        BwtLogger::currLogger()->debug($mn, $msg);
    }

    public static function logError($mn, $ex)
    {
        BwtLogger::currLogger()->error($mn);
    }
    // </editor-fold>
}
