<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : BwtController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/bwt' . $delim . '/home/iordanov/common/lib/bwt/models' .
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "BwtController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("BwtConnection.php");
require_once("BwtLogger.php");
require_once("Functions.php");
require_once("BwtRestHandlerAccount.class.php");
require_once("BwtStorylineModel.class.php");
require_once("BwtStorylinePubModel.class.php");
require_once("BwtCookieConsentModel.class.php");

BwtLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
BwtLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new BwtRestHandlerAccount();
    $restHendler->Option();
    BwtLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            $restHendler->Ping($id);
            BwtLogger::log($mn, "ping executed");
            break;
        // <editor-fold defaultstate="collapsed" desc="Roadmap Methods">
        
        case "bwt_roadmap_get":
            // to handle REST Url /pcpd/
            $rh = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //BwtLogger::log($mn, " bookId: " . $payload_json->bookId . " ");
                if (isset($payload_json)){
                    $response = BwtStorylineModel::StorylineTable($payload_json, null);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Storyline Pub Methods">
        
        case "roadmap_view":
            $rh = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                if (!isset($payload_json->guid)){
                    $response = new Response("error", 'No public record found for provided value.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                BwtLogger::log($mn, " guid: " . $payload_json->guid . " ");
                $stlpub = BwtStorylinePubModel::LoadByGuid($payload_json->guid);
                if(!isset($stlpub) || !isset($stlpub->id) || !isset($stlpub->storylineId)){
                    $response = new Response("error", 'No public record found for provided value.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                $visitor = new BwtStorylinePubViewModel();
                
                $visitor->stlpubId = $stlpub->id;
                $visitor->storylineId = $stlpub->storylineId;
                $visitor->username = isset($payload_json->username)?$payload_json->username:$remoteIp;
                $visitor->ipaddress = $remoteIp;
                $visitor = BwtStorylinePubViewModel::Save($visitor);
                BwtLogger::log($mn, " storylineId: " . $stlpub->storylineId . " ");
                $response = BwtStorylineModel::PublicRoadmapGet($stlpub->storylineId);
                $rh->EncodeResponce($response);
                return;

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "bwt_storylinepubview_save":
            $rh = new BwtRestHandlerAccount();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                if(isset($dataJson->storylinepubview)){
                    $pubview = $dataJson->storylinepubview;
                    $pubview->ipaddress = $remoteIp;
                    if(!isset($pubview->username)){$pubview->username = $remoteIp;}
                    $rh->StorylinePubViewSave($pubview->storylinepubview);
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            }
            break;
         
        case "bwt_storyline_pub_get_id":
            $rh = new BwtRestHandlerAccount();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubGetById($dataJson->id);
            }
            break;
            
         // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Storyline Pub Methods">

            case "bwt_consent_save":
            $rh = new BwtRestHandlerAccount();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                if(isset($dataJson->consent)){
                    $pubview = $dataJson->consent;
                    $pubview->ipaddress = $remoteIp;
                   
                    $rh->BwtCookieConsentSave($pubview);
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            }
            break;
         
        case "bwt_consent_get_id":
            $rh = new BwtRestHandlerAccount();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->BwtCookieConsentGetById($dataJson->id);
            }
            break;
            
         // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ML Methods">
            
        case "mlload":
            $dataPayload = json_decode(file_get_contents('php://input'));
            BwtRestHandlerAccount::MlLoad($dataPayload);
            break;
        case "home":
            $dataPayload = json_decode(file_get_contents('php://input'));
            BwtRestHandlerAccount::HomeMembersOnline(8);
            break;
        case "ml":
            $dataPayload = json_decode(file_get_contents('php://input'));
            switch ($method) {
                case 'PUT':
                    BwtRestHandlerAccount::MlUpdate($dataPayload);
                    break;
                case 'POST':
                    BwtRestHandlerAccount::MlInsert($dataPayload);
                    break;
                case 'DELETE':
                    BwtRestHandlerAccount::MlDelete($dataPayload);
                    break;
            }
            break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="BWT User Methods">
        
        case "login":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                //BwtLogger::log($mn, "[login] dataJson: " . $dataJson->username . " ");
                 $restHendler->Login($dataJson->username, $dataJson->password);
            }
            break;
        case "bwt-login":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $dataJson = json_decode($payload);
                //BwtLogger::log($mn, "[login] dataJson: " . $dataJson->username . " ");
                 $restHendler->BwtLogin($dataJson->email, $dataJson->password);
            }
        break;
        case "bwt-user-save":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $dataJson = json_decode($payload);
                //BwtLogger::log($mn, "[login] dataJson: " . json_encode($dataJson) . " ");
                 $restHendler->BwtUserSave($dataJson->user);
            }
        break;
        case "bwt-pwd-change":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $dataJson = json_decode($payload);
                //BwtLogger::log($mn, "[login] dataJson: " . json_encode($dataJson) . " ");
                 $restHendler->BwtChangePassword($dataJson);
            }
        break;
        case "bwt-sign-up":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $dataJson = json_decode($payload);
                //BwtLogger::log($mn, "[login] dataJson: " . json_encode($dataJson) . " ");
                 $restHendler->BwtRegister($dataJson->email, $dataJson->password);
            }
        break;        
            
        case "refreshtoken":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $restHendler->refreshToken($dataJson->refresh);
            } else{
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }

            break;
        case "book_autor":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                BwtLogger::log($mn, "[login] dataJson: " . $dataJson->user_id . " ");
                 $restHendler->Autor($dataJson->user_id);
            }
            break;
        case "register":
            // to handle REST Url /pcpd/
            $restHendler = new BwtRestHandlerAccount();
            // read JSon input
             $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
            
                BwtLogger::log($mn, "[Register] dataJson: " . $dataJson->email . " ");
                $restHendler->Register($dataJson->email, $dataJson->password);
            }
                

            break;
       // </editor-fold>
        default:
            BwtLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


BwtLogger::logEnd($mn);

