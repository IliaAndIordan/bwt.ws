<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: BwtBookRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: BwtBookRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("BwtConnection.php");
require_once("BwtLogger.php");
require_once("JwtAuth.php");
require_once("User.class.php");
require_once("UserModel.class.php");
require_once("Character.class.php");
require_once("Chapter.class.php");
require_once("Book.class.php");
require_once("BookModel.class.php");
require_once("BwtCharacterModel.class.php");
require_once("BwtStorylineModel.class.php");
require_once("BwtStorylinePubModel.class.php");


/**
 * Description of BwtBookRestHandler
 *
 * @author IZIordanov
 */
class BwtBookRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">

    public function Option()
    {
        $mn = "BwtBookRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new BwtBookRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "BwtBookRestHandler::Ping()";
        BwtLogger::logBegin($mn);
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            if (isset($conn)) {
                BwtLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="Book Old">
    
    public function UserBookList($userId) {
        $mn = "BwtBookRestHandler::UserBookList(".$userId.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT book_id as id, 
                        book_title as title,
                        book_subtitle as subtitle,
                        book_coverpage_url as coverpageUrl,
                        book_type_id as typeId,
                        book_desc as description,
                        user_id as userId,
                        adate, udate
                    FROM iordanov_bwt.bwt_book
                    where user_id = ?
                    order by udate desc ";
            $bound_params_r = ["i", $userId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("book_list",$ret_json_data);
            
        } catch (Exception $ex) {
             BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BookSave($book) {
        $mn = "BwtBookRestHandler::BookSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            
            $obj = new Book();
            $obj->loadById($book->id);
            $obj->title = $book->title;
            $obj->subtitle = $book->subtitle;
            $obj->coverpageUrl = $book->coverpageUrl;
            $obj->userId = $book->userId;
            $obj->typeId = $book->typeId;
            $obj->description = $book->description;
            
            $obj->save();
            
            $obj->loadById($book->id);
            $response->addData("book",$obj);
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BookModel New">
    
    public function BookGet($value) {
        $mn = "BwtBookRestHandler::BookGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtBookModel::LoadById($value);
            $response = new Response("success", "BwtBookModel get.");
            $response->addData("book", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BookModelSave($value) {
        $mn = "BwtBookRestHandler::BookModelSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtBookModel::Save($value);
            $response = new Response("success", "Charter data saved.");
            $response->addData("book", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BookModelDelete($id) {
        $mn = "BwtBookRestHandler::BookModelDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtBookModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "CfgCharter deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Characters">
    
    public function BookCharacterList($book_id) {
        $mn = "BwtBookRestHandler::BookCharacterList(".$book_id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT crtr_id, 
                crtr_name, crtr_nikname, crtr_url, 
                book_id, crtr_desc, 
                crtr_order, crtr_image_url
                FROM iordanov_bwt.bwt_character
                WHERE book_id=?
                ORDER BY crtr_order, crtr_name, crtr_nikname";
            $bound_params_r = ["i", $book_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("book_chrtr_list",$ret_json_data);
            
        } catch (Exception $ex) {
             BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BookCharacterSave($crtr) {
        $mn = "BwtBookRestHandler::BookCharacterSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            
            $obj = new Character();
            if(isset($crtr->crtr_id)){
                $obj->loadById($crtr->crtr_id);
            }
            
            $obj->crtr_name = $crtr->crtr_name;
            $obj->crtr_nikname = $crtr->crtr_nikname;
            $obj->crtr_url = $crtr->crtr_url;
            $obj->book_id = $crtr->book_id;
            $obj->crtr_desc = $crtr->crtr_desc;
            $obj->crtr_order = $crtr->crtr_order;
            $obj->crtr_image_url = $crtr->crtr_image_url;
            
            $obj->save();
            $response->addData("crtr",$obj);
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BwtCharacters">
    
    
    public function CharacterGet($value) {
        $mn = "BwtBookRestHandler::CharacterGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtCharacterModel::LoadById($value);
            $response = new Response("success", "BwtCharacterModel get.");
            $response->addData("character", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CharacterSave($value) {
        $mn = "BwtBookRestHandler::CharacterSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtCharacterModel::Save($value);
            $response = new Response("success", "Character data saved.");
            $response->addData("character", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CharacterDelete($id) {
        $mn = "BwtBookRestHandler::CharacterDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtCharacterModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Character deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Chapter">
    
    public function BookChapterList($book_id) {
        $mn = "BwtBookRestHandler::BookCharacterList(".$book_id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT bch_id, bch_title, bch_subtitle,
            bch_notes, bch_content, bch_order, adate, udate, book_id
            FROM iordanov_bwt.bwt_book_chapter
            WHERE book_id = ?
            order by bch_order, bch_id";
            $bound_params_r = ["i", $book_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("book_chapter_list",$ret_json_data);
            
        } catch (Exception $ex) {
             BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BookChapterSave($chapter) {
        $mn = "BwtBookRestHandler::BookChapterSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            
            $obj = new Chapter();
            if(isset($chapter->bch_id)){
                $obj->loadById($chapter->bch_id);
            }
            
            $obj->bch_title = isset($chapter->bch_title)?$chapter->bch_title:null;
            $obj->bch_subtitle = isset($chapter->bch_subtitle)?$chapter->bch_subtitle:null;
            $obj->bch_notes = isset($chapter->bch_notes)?$chapter->bch_notes:null;
            $obj->book_id = $chapter->book_id;
            $obj->bch_content = isset($chapter->bch_content)?$chapter->bch_content:null;
            $obj->bch_order = $chapter->bch_order;
            
            $obj->save();
            
             $sql = "SELECT bch_id, bch_title, bch_subtitle,
            bch_notes, bch_content, bch_order, adate, udate, book_id
            FROM iordanov_bwt.bwt_book_chapter
            WHERE book_id = ? and bch_id = ?
            order by bch_order, bch_id";
            $bound_params_r = ["ii", $obj->book_id, $obj->bch_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("chapter",$ret_json_data[0]);
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="BwtStorylineModel">
    
    
    public function StorylineGet($value) {
        $mn = "BwtBookRestHandler::StorylineGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylineModel::LoadById($value);
            $response = new Response("success", "BwtCharacterModel get.");
            $response->addData("storyline", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylineSave($value) {
        $mn = "BwtBookRestHandler::StorylineSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylineModel::Save($value);
            $response = new Response("success", "Character data saved.");
            $response->addData("storyline", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylineDelete($id) {
        $mn = "BwtBookRestHandler::StorylineDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtStorylineModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Storyline deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BwtStorylinePubModel">
    
    public function StorylinePubGet($value) {
        $mn = "BwtBookRestHandler::StorylinePubGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubModel::LoadByGuid($value);
            $response = new Response("success", "BwtStorylinePubModel get.");
            $response->addData("storyline", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylinePubSave($value) {
        $mn = "BwtBookRestHandler::StorylinePubSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubModel::Save($value);
            $response = new Response("success", "Storyline data published.");
            $response->addData("storylinePub", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylinePubDelete($id) {
        $mn = "BwtBookRestHandler::StorylinePubDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtStorylinePubModel::DeleteStotyline($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Storyline Pub deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BwtStorylinePubViewModel">
    
    public function StorylinePubViewGet($value) {
        $mn = "BwtBookRestHandler::StorylinePubViewGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubViewModel::LoadByid($value);
            $response = new Response("success", "BwtStorylinePubViewModel get.");
            $response->addData("storylinepubview", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylinePubViewSave($value) {
        $mn = "BwtBookRestHandler::StorylinePubViewSave()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BwtStorylinePubViewModel::Save($value);
            $response = new Response("success", "BwtStorylinePubViewModel data published.");
            $response->addData("storylinepubview", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function StorylinePubViewDelete($id) {
        $mn = "BwtBookRestHandler::StorylinePubViewDelete(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtStorylinePubViewModel::Delete($id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Storyline Pub deleted.");
            $response->addData("id", $id);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
}
