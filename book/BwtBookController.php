<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : BwtBookController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') . $delim . '../' .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/bwt' . $delim . '/home/iordanov/common/lib/bwt/models' .
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "BwtBookController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("BwtLogger.php");
require_once("Functions.php");
require_once("BookModel.class.php");
require_once("BwtCharacterModel.class.php");
require_once("BwtStorylineModel.class.php");
require_once("BwtBookRestHandler.class.php");
require_once("BwtStorylinePubModel.class.php");

BwtLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
BwtLogger::log($mn, "view=" . $view);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new BwtBookRestHandler();
    $restHendler->Option();
    BwtLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
   $method = $_SERVER['REQUEST_METHOD'];
   BwtLogger::log($mn, "method=" . $method);
    // read JSon input
    $payloadJson;
    $payloadAuth;
    $actor;
    $actor_id;
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    
    $authRes = JwtAuth::Autenticate();
    
     if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
            if(isset($payloadAuth)){
                $actor_id = $payloadAuth->data->user_id;
                $actor = $payloadAuth->data->user;
                BwtLogger::log($mn, "ping actor_id: ".$actor_id);
                BwtLogger::log($mn, "ping actor".json_encode($actor));
            }
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new BwtBookRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new BwtBookRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
    try{
        $payload = file_get_contents('php://input');
        if(isset($payload)){
            $payloadJson = json_decode($payload);
            BwtLogger::log($mn, "payloadJson=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn,  $ex);
    }
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new BwtBookRestHandler();
            $restHendler->Ping($id);
            BwtLogger::log($mn, "ping executed");
            break;
        
        // <editor-fold defaultstate="collapsed" desc="Book Old and New Methods">
        
        case "book_list":
            $restHendler = new BwtBookRestHandler();
            $response = array();
            try {
                
                $restHendler->UserBookList($payloadJson->user_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response) );
            break;
        case "bwt_book_get":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->BookGet($dataJson->id);
            }
            break;
            
        case "bwt_book_save":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->BookModelSave($dataJson->book);
            }
            break;
        
        case "bwt_book_delete":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->BookModelDelete($dataJson->id);
            }
            break;
            
        case "book_table":
            // to handle REST Url /pcpd/
            $rh = new BwtBookRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                BwtLogger::log($mn, " qry_limit: " . $payload_json->limit . " ");
                if (isset($payload_json)){
                    $response = BookModel::BooksTable($payload_json, $actor_id);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        case "book_save":
            // to handle REST Url /pcpd/
            $restHendler = new BwtBookRestHandler();
            $response = array();
            try {
                
                $restHendler->BookSave($payloadJson->book);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response));
            break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Character Methods Old">
        
        case "book_character_list":
            $restHendler = new BwtBookRestHandler();
            $response = array();
            try {
                
                $restHendler->BookCharacterList($payloadJson->book_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response));
            break;
        case "book_character_save":
            // to handle REST Url /pcpd/
            $restHendler = new BwtBookRestHandler();
            $response = new Response();
            try {
                
                $restHendler->BookCharacterSave($payloadJson->crtr);
            } catch (Exception $ex) {
               $response = new Response("error", 'Book data not saved.');
                //$response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response));
            break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Character Methods">
        
        case "bwt_character_get":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CharacterGet($dataJson->id);
            }
            break;
            
        case "bwt_character_save":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CharacterSave($dataJson->chapter);
            }
            break;
        
        case "bwt_character_delete":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CharacterDelete($dataJson->id);
            }
            break;
            
        case "bwt_character_table":
            // to handle REST Url /pcpd/
            $rh = new BwtBookRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                BwtLogger::log($mn, " qry_limit: " . $payload_json->limit . " ");
                if (isset($payload_json)){
                    $response = BwtCharacterModel::CharacterTable($payload_json, $actor_id);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
         // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Storyline Methods">
        
        case "bwt_storyline_get":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylineGet($dataJson->id);
            }
            break;
            
        case "bwt_storyline_save":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylineSave($dataJson->storyline);
            }
            break;
        
        case "bwt_storyline_delete":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylineDelete($dataJson->id);
            }
            break;
            
        case "bwt_storyline_table":
            // to handle REST Url /pcpd/
            $rh = new BwtBookRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                BwtLogger::log($mn, " qry_limit: " . $payload_json->limit . " ");
                if (isset($payload_json)){
                    $response = BwtStorylineModel::StorylineTable($payload_json, $actor_id);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Storyline Pub Methods">
        
        case "bwt_storyline_pub_get":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubGet($dataJson->guid);
            }
            break;
        
        case "bwt_storyline_pub_save":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubSave($dataJson->storyline);
            }
            break;
            
        case "bwt_storyline_pub_delete":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubDelete($dataJson->storylineId);
            }
            break;
            
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Storyline Pub View Methods">
            
        case "bwt_storylinepubview_get":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubViewGet($dataJson->id);
            }
            break;
        
        case "bwt_storylinepubview_save":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubViewSave($dataJson->storylinepubview);
            }
            break;
            
        case "bwt_storylinepubview_delete":
            $rh = new BwtBookRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->StorylinePubViewDelete($dataJson->id);
            }
            break;
            
        case "bwt_storylinepubview_table":
            // to handle REST Url /pcpd/
            $rh = new BwtBookRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                //BwtLogger::log($mn, " qry_limit: " . $payload_json->limit . " ");
                if (isset($payload_json)){
                    $response = BwtStorylinePubViewModel::StorylinePubViewTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
         // </editor-fold>
        
        case "book_chapter_list":
            $restHendler = new BwtBookRestHandler();
            $response = array();
            try {
                
                $restHendler->BookChapterList($payloadJson->book_id);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response));
            break;
        case "book_chapter_save":
            // to handle REST Url /pcpd/
            $restHendler = new BwtBookRestHandler();
            $response = array();
            try {
                
                $restHendler->BookChapterSave($payloadJson->chapter);
            } catch (Exception $ex) {
                $response = array("status" => "success", "data" => array(), "message" => "Error: " . $ex->getMessage());
            }
            BwtLogger::log($mn, "response= " . json_encode($response));
            break;
        
        default:
            BwtLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


BwtLogger::logEnd($mn);

